#!/bin/bash
#export PASSPHRASE=SomeLongGeneratedHardToCrackKey

duplicity full --no-encryption \
 --archive-dir /mnt/fins_app_backup/duplicity/cache/ \
 --verbosity=8 \
 --include=/mnt/fins_app_media/media \
 --exclude=/** \
 / file:///mnt/fins_app_backup/media

find /mnt/fins_app_backup/media -name "duplicity*.gz" -mtime +7 -type f -delete
find /mnt/fins_app_backup/media -name "duplicity*.manifest" -mtime +7 -type f -delete

#unset PASSPHRASE
