<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Industry extends Model
{
    protected $fillable = [
        'name',
        'order',
        'isActive'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];

    public function posts()
    {
        return $this->hasMany(Post);

    }


}
