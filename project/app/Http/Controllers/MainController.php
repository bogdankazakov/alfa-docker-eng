<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DemoRequest;


class MainController extends Controller
{
    public function index()
    {
        return view('mainpage.main');
    }
}
