<?php

namespace App\Http\Controllers;

use App\Post;
use App\Quizz;
use App\Industry;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

class PostExtraController extends Controller
{


    public function addlike(Request $request)
    {
        if ($request->input('type') === 'post') {
            $post = Post::findOrFail($request->input('id'));
            $total = $post->likes  +  1;
            $post ->likes = $total;
            $post->save();
            return [
                'total' =>  $total,
            ];
        } else{
            $quizz = Quizz::findOrFail($request->input('id'));
            $total = $quizz->likes  +  1;
            $quizz ->likes = $total;
            $quizz->save();
            return [
                'total' =>  $total,
            ];
        }

    }

    public function addview(Request $request)
    {
        if ($request->input('type') === 'post') {
            $post = Post::findOrFail($request->input('id'));
            $total = $post -> views +  1;
            $post -> views = $total;
            $post->save();

            return [
                'total' =>  $total,
            ];
        } else{
            $quizz = Quizz::findOrFail($request->input('id'));
            $total = $quizz -> views +  1;
            $quizz -> views = $total;
            $quizz->save();

            return [
                'total' =>  $total,
            ];
        }

    }
    public function imageupload(Request $request)
    {
        $paths = array();
            foreach ($request->file() as $file) {
                $path = $file->store('postsbody', 'public');
                array_push($paths, $path);
            }
        return $paths;
    }

}
