<!-- MENU OPEN -->
<transition  name="fade">
    <section class="menu" v-show="showMenu">
        <div class="menu-bg"></div>
        <div class="container menu-body">
            <div class="row">
                <div class="col-12" >
                    <button class=" login">
                        <img class="mr-3" src={{ asset('icons/user_icon.svg') }}>
                        Войти в личный кабинет
                    </button>
                    <subscription></subscription>
                </div>
            </div>
        </div>
    </section>
</transition>

<transition name="fade">
    <search
        v-bind:show="showSearch"
        v-on:searchclose="closeSearch"
        v-show="showSearch"
        v-bind:btn-back-url="'{{ route('index') }}'"
        v-bind:logo-url="'{{ asset('img/logo.svg') }}'"
        v-bind:logo-mini-url="'{{ asset('img/logomini.svg') }}'"
        v-bind:icons-close-url="'{{ asset('icons/close_icon.svg') }}'"
        v-bind:icons-heart-url="'{{ asset('icons/heart_icon.svg') }}'"
        v-bind:icons-view-url="'{{ asset('icons/view_icon.svg') }}'"
        v-bind:icons-comment-url="'{{ asset('icons/comment_icon.svg') }}'"
    ></search>
</transition>





<!-- MODAL AUTH -->
<transition  name="fade" >
    <auth
        v-on:authenticated="onAuthentication"
        v-on:logout="isAuth = false"
        v-bind:show-auth="showAuth"
        v-bind:show-logout.sync="showLogout"
        v-bind:close-icon-url="'{{ asset('icons/close_icon.svg') }}'"
        v-on:close-modal="closeModal('auth')"

    ></auth>
</transition>


<!-- MODAL SUBSCRIBE -->
<transition  name="fade" >
    <section v-if="showSubscribe" class="modal-container">
        <div class="modal-container-bg"  v-on:click="closeModal('subscribe')"></div>
        <div class="project-modal">
            <img class="project-modal-close-btn" src={{ asset('icons/close_icon.svg') }} v-on:click="closeModal('subscribe')">
            <subscription></subscription>
        </div>
    </section>
</transition>
