@foreach ($post_render_ready as $block)

    <div class="postedit-container">
        <div class="postedit-content">
            @if ($block->typeId === 0)
                <div>
                    {!! $block->content->text !!}
                </div>
            @elseif ($block->typeId === 1)
                <div>
                    <img class="article"  src="{{ asset('storage/'.$block->content->headerimage) }}" />
                </div>
            @elseif ($block->typeId === 2)
                <div style="padding: 1px 0;">
                    <img class="article-txt-image"  src="{{ asset('storage/'.$block->content->headerimage) }}" />
                    <div class="article-txt-image-caption">
                        {!! $block->content->imagedescription ?? '' !!}
                    </div>
                </div>
            @elseif ($block->typeId === 3)
                <div class="article-txt-mark-1">
                    {!! $block->content->text !!}
                </div>
            @elseif ($block->typeId === 4)
                <div class="article-txt-mark-2">
                    {!! $block->content->text !!}
                </div>
            @elseif ($block->typeId === 5)
                <div class="article-video">
                    {!! $block->content->text !!}
                </div>
            @endif
        </div>
    </div>

@endforeach
