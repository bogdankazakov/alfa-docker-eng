<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/moment-with-locales.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('font/proximanova/stylesheet.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet"> -->
</head>
<body class="body">
    <div id="app">
        <!-- MAIN MENU -->
        <nav class="navbar navbar-expand-md navbar-light nav-padding fixed-top  nav-project" v-show="!showSearch">
            <div class="container">

                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="header-logo navbar-collapse collapse" src={{ asset('img/logo.svg') }}>
                    <img class="navbar-toggler header-logo-mini " src={{ asset('img/logomini.svg') }}>
                </a>


                <ul class="navbar-nav">
                    <div class="d-flex pl-3 align-items-center switcher-container">
                        <span >Журнал</span>
                        <label class="switch mx-3 mt-2">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                        <span class="text-secondary">Город</span>
                    </div>
                </ul>

                <button class="navbar-toggler" type="button" v-on:click="showMenu = true" >
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#" v-on:click="showSearch = !showSearch">
                                    <img class="header-icon" src={{ asset('icons/search_icon.svg') }}>
                                    <span>Поиск</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">
                                    <img class="header-icon" src={{ asset('icons/user_icon.svg') }}>
                                    <span>Войти</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" v-on:click="showSubscribe = true">
                                    <span>Подписка</span>
                                </a>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- SEARCH MENU -->
        <nav class="navbar navbar-expand-md navbar-light nav-padding fixed-top  nav-project bg-white" v-show="showSearch">
            <div class="container d-flex flex-nowrap">
                <a class="navbar-brand d-none d-sm-block" href="{{ url('/') }}">
                    <img class="header-logo navbar-collapse collapse" src={{ asset('img/logo.svg') }}>
                    <img class="navbar-toggler header-logo-mini " src={{ asset('img/logomini.svg') }}>
                </a>
                <form class="my-auto d-inline w-100 ml-sm-5">
                    <input class="form-control search-input" type="search" v-model="searchQuery" placeholder="Поиск">
                </form>
                <img class="search-close-btn" src={{ asset('icons/close_icon.svg') }} v-on:click="showSearch = !showSearch; searchResults = {}">
            </div>
        </nav>

        <!-- OPEN MENU -->
        <nav class="navbar navbar-expand-md navbar-light nav-padding fixed-top  nav-project" v-show="showMenu">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="header-logo navbar-collapse collapse" src={{ asset('img/logo.svg') }}>
                    <img class="navbar-toggler header-logo-mini " src={{ asset('img/logomini.svg') }}>
                </a>
                <div>
                    <img class="menu-search-btn" src={{ asset('icons/search_icon.svg') }} v-on:click="showMenu = false; showSearch = true;">
                    <img class="menu-close-btn" src={{ asset('icons/close_icon.svg') }} v-on:click="showMenu = false">
                </div>
            </div>
        </nav>

        @yield('content')

    </div>

    @yield('scripts')
</body>
</html>
