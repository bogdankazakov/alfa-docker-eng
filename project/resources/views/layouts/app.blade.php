@extends('layouts.base')

@section('body')
<body >
    <div id="app">

        <!--Preloader-->
        <transition  name="fade">
            <div class="loaderArea" v-if="pageLoading">
                <div class="loader">
                    <div class="spinner-grow text-light" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </transition>
        <!--End-->

        <div  v-bind:class="classBody"
            tabindex="-1"
            v-on:keydown.shift.56.capture.prevent.stop="activateLogout"
        >
            @if(! empty($post))
                @include('layouts.menu-article')
            @elseif(! empty($quizz))
                @include('layouts.menu-quizz')
            @else
                @include('layouts.menu-main')
            @endif
            @include('layouts.navbasic')

            @yield('content')


        </div>
    </div>
    @yield('scripts')
</body>
@endsection
