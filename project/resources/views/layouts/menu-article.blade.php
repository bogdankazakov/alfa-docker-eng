
        <!-- MAIN MENU -->
        <nav class="navbar navbar-expand-md navbar-light nav-padding fixed-top" v-bind:class="classNav" v-show="!showSearch">
            <div class="container">

                <a class="navbar-brand" href="{{ route('index', []) }}">
                    <img class="header-logo navbar-collapse collapse" src={{ asset('img/logo.svg') }}>
                    <div  class="back navbar-toggler" >
                        <img class="mr-3" src={{ asset('icons/back.svg') }}>
                        <span>ALL NEWS</span>
                    </div>
                </a>





                <button class="navbar-toggler" type="button" v-on:click="showMenu = true" >
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#" v-on:click="openSearch">
                                    <img class="header-icon" src={{ asset('icons/search_icon.svg') }}>
                                    <span>Search</span>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <img class="header-icon" src={{ asset('icons/user_icon.svg') }}>
                                    <span>Войти</span>
                                </a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="#" v-on:click="openModal('subscribe')">
                                    <span>Subscribe</span>
                                </a>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>
