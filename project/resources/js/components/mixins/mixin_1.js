export default {
    methods: {
        postFormater: function(list){
            for (const item of list){
                item.published_at = moment(item.published_at  + ' Z').locale('en').format("D MMM");

                item.tags = []
                var category = null

                try {
                   var img_arr = item.image.split(".");
                   img_arr.splice(1, 0, '-small');
                   img_arr[img_arr.length - 1] = "."+img_arr[img_arr.length - 1]
                   item.image = '/storage/' + img_arr.join("");
                }
                catch (e) {}

                if(item.industry !== null ){
                    item.tags.push(
                        {
                            'id': 0,
                            'name': item.industry.name
                        }
                    )
                }

                if(item.category !== null){
                    item.tags.push(
                        {
                            'id': 0,
                            'name': item.category.name
                        }
                    )
                }
            }
            return list
        },
        setLink:function(post){
            if (post.hasOwnProperty("author_id")){
                return '/posts/' + post.id
            }  else{
                return '/quizz/' + post.id
            }
        },
    }
};
